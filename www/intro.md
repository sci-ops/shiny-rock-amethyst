---
title: "Intro"
output: html_document
---

# Welcome to this Shiny ROCK app!

This Shiny App was developed for cognitive interviews.

It allows you to upload one or more ROCK sources, parse them into a qualitative data table, and produce a Cognitive Interview heatmap. This heatmap shows which codes occur for which items.

The app assumes you followed the ROCK conventions, using double square brackets to denote code identifiers; and that you use the `ci--` prefix to denote cognitive interviewing codes and the `uiid:` prefix to denote which items data relates to (`uiid` stands for Unique Item Identifier).

For example, the following source can be succesfully parsed:

```
[[cid:1]]

###-----------------------------------------------------------------------------

How large is your family? [[uiid:familySize_7djdy62d]]

Participant also counts 3 dogs and 7 goldfish [[ci--understanding]]

Participant does not count own brothers and sisters, only their partner and children [[ci--retrieval]]

###-----------------------------------------------------------------------------

How many windows are there in your house? [[uiid:windows_7djdy62d]]

Participant does not live in a house, but in an apartment [[ci--content_adequacy]]

Participant also counts windows in doors inside the house [[ci--understanding]]

###-----------------------------------------------------------------------------
```

The app will ignore class instance identifiers other than `uiid`, so case identifiers (e.g., `[[cid:1]]`) are ignored.
